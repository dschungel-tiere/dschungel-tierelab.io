---
template: brand-banner.html
---

# Meine Dschungeltiere

Hier siehst du verschiedene Tierarten welche im Dschungel leben.

## Tiger

![Tiger im Dschungel](tiger-im-dschungel.jpg){ width="400", align=right }

Der Tiger ist eine Raubkatze, und nämlich die grösste die es gibt. Er Lebt in den Gebieten von China, Russland, Indien, Thailand, Malaysia und Indonesien.
Der Tiger ist mein Lieblingstier.
Darum finde ich es besonders Schade das er auf der roten Liste ist. Das bedeuted das er vom aussterben bedroht ist.
WWF hat ein Projekt am laufen, das die Tiger am Leben bleiben.

<br><br><br>
## Roter Panda

![Roter Panda im Dschungel](little-panda.jpg){ width="400", align=right }

Der Rote Panda ist ein kleines, süsses und quirliges Tier. Wie der Panda und der Koala besteht seine Haubtnahrung aus Bambus. Er lebt in dem Dschungel Chinas. Wie der Tiger ist er auf der Roten Liste.
 Der Rote Panda ist etwa 1m lang. Wilderer können sein Fell teuer verkaufen, und darum wird er von ihnen gejagt.

<br><br><br>
## Inlandtaipan

![Inlandtaipan im Dschungel](inlandtaipan.jpg){ width="400", align=right }

Der Inlandtaipan ist die Giftigste Schlange der Welt. Er kann mit einem Biss 250 Menschen Töten.
Er ist in Australien heimisch. Er ist circa 2,5m lang, sie ist zudem die einzige Schlange die ihre Farbe verändern kann. 

<br><br><br><br><br><br>

## Waldbrände

![Waldbrand im Dschungel](Waldbrand.jpg){ width="400", align=right }

Nicht nur Wilderer sind für die Tiere gefährlich. Sondern auch Waldbrände machen ihnen zu schaffen. Und oft hat an den Waldbränden auch der Mensch Schuld, denn für die Papierproduktion und für die Felder um Ackerbau zu betreiben muss er den Wald abroden. Dafür wird oft die Brandrodung angewandt. Und die Ackerbauprodukte solcher Felder braucht der Mensch für die Massentierhaltung. Und somit leiden nicht nur die Dschungeltiere, sondern auch die Nutztiere (Massentierhaltung).

Diesen Tieren kann man helfen, wenn man schaut welches Papier man kauft und sich achtet, dass man gute Produkte kauft oder vielleicht sogar seine Ernährung umstellt auf vegan.


## Danke

Diese Website war ein Zukunfttagsprojekt. Danke das du bis hierhin gelesen hast. ;-)
