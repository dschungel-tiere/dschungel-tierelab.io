document$.subscribe(function() {
  var tables = document.querySelectorAll("article table")
  tables.forEach(function(table) {
    new Tablesort(table)  // make tables sortable
    makeTableSearchable(table)  // make tables searchable
  })
})
