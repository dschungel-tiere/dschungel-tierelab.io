function makeTableSearchable(table) {
  const input = makeTableSearchInput(table)
  const container = document.createElement('div')
  container.appendChild(input)
  table.parentNode.insertBefore(container, table);
}

function makeTableSearchInput(table) {
  const input = document.createElement('input');
  input.classList.add('table-search__input')
  input.addEventListener('keyup', () => filterTable(input.value, table));
  input.placeholder = "Search table...";
  return input
}

function filterTable(query, table) {
  const filter = query.toUpperCase();
  const body = table.querySelector('tbody')
  const rows = body.getElementsByTagName("tr");

  // Loop through all table rows and columns, and hide those who don't contain the search query
  var cols, row, td, i, j, txtValue, isRowVisible;
  for (i = 0; i < rows.length; i++) {
    row = rows[i];
    isRowVisible = false;
    cols = row.getElementsByTagName("td");
    for (j = 0; j < cols.length; j++) {
      td = cols[j]
      if (td) txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) isRowVisible = true
    }
    row.style.display = isRowVisible ? "" : "none";
  }
}
